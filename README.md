## Minio 2020
**docker-compose.yml**
```
docker-compose up --build -d
```
## Minio 2023
**minio-single-update.yml**
```
docker-compose -f minio-single-update.yml up --build -d
```

**minio-multiple.yml**
```
docker-compose -f minio-multiple.yml up --build -d
```

## Note
- Jika menggunakan volume, isi pada folder yang digunakan harus kosong
- Jika menggunakan volume, berikan permission pada folder yang digunakan
```
sudo chown 1001:$USER <nama-folder>
```